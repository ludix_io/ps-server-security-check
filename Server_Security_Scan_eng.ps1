﻿<#
Title: Server Security Check
Author: I. B.
Version: 1.2
Date: 02.10.2022
Reference: VDA @ IPSO 18.10.2022, HFSNT.H.4.24-ZH

Description:
This script checks all servers in a specified OU. Then it creates a report per check in the path defined under $outputfiledirdef.

Checks and reports:
  - Report about OS version and patch level information
  - Report about non-standard SMB shares
  - Report about services which are executed with non-standard accounts
  - Report about installed software and its version
  - Report about PowerShell Execution Policy (ByPass, RemoteSignes etc.)
  - Report about Scheduled Task which are executed with non-standard accounts

Changelog:
  - Rudimentary basic function (B1)
  - CSV reports improved (B1)
  - After the checks a logfile with summary is written (B1)
  - Checks extended (V1.0)
  - Reports are compressed to ZIP file for easier reuse (V1.0)
  - Checks can now be selected and deselected individually (V 1.1)
  - Performance increased by Try/Catch (V1.1)
  - Logfile now also shows successful systems (V1.1)
  - Code commented (V1.1)
  - Citrix VDA OU can now be defined, also a single VDA if they are identical (V1.2)

#>

# Start of variables to define ---------------------------------------------

# OU of Servers in AD
$serveroudef = "OU=Servers,DC=XXX,DC=local"

# OU of DCs in AD
$dcserveroudef = "OU=Domain Controllers,DC=XXX,DC=local"

# OU of the Citrix VDAs in the AD. Can also be left empty and is then skipped (even if $ctxvdaexampldef is defined).
$ctxvdaoudef = "" # Example: "OU=CTX_VDA,DC=XXX,DC=local"

# If PVS or a Golden Image is in use, a specified single VDA can be added to the scan, as they are identical. $ctxvdaoudef must be mandatory filled, even if only a single VDA is to be scanned.
$ctxvdaexampldef = "" # Example: "vda01"

# Output Directory
$outputfiledirdef = "C:\serversecuritychecks\"

# End of variables to define -------------------------------------------------

# Select checks (1 = Enabled, 0 = Disabled)
$run_osinfo =    1   # report about OS version and patch level information
$run_smbinfo =   1   # report about non-standard SMB shares
$run_svcinfo =   1   # report about services running with non-standard accounts
$run_swinfo =    1   # report about installed software and its version
$run_execinfo =  1   # report about PowerShell Execution Policy (ByPass, RemoteSignes etc.)
$run_taskinfo =  1   # report about Scheduled Task which are executed with non-standard accounts

# Global predefined variables

$computers = ""
$server_ou = ""
$dc_ou = ""
$successfull = @()  # Created as an array, otherwise a single string would be created from the jointed objects
$errors = @()       # Created as an array, otherwise a single string would be created from the jointed objects

# Create folder for reports if not already existing

if (-not (test-path -path $outputfiledirdef)) {
    New-Item -ItemType directory -Path $outputfiledirdef
}

$outputfile = $outputfiledirdef + "Check_from_$(get-date -UFormat %Y%m%d_%H-%M)"
$logfile = $outputfiledirdef + "Check_from_$(get-date -UFormat %Y%m%d_%H-%M)_log.txt"


# Query names of servers and DCs from AD and store them in variables
$server_ou = Get-ADComputer -filter * -SearchBase $serveroudef | select-object Name
$dc_ou = Get-ADComputer -filter * -SearchBase $dcserveroudef | select-object Name
$computers = $server_ou + $dc_ou

if ($ctxvdaoudef -and $ctxvdaexampldef){
    $computers += (get-adcomputer -filter {name -like $ctxvdaexampldef} -searchbase $ctxvdaoudef)
}

foreach ($computername in $computers) {
    try {
        invoke-command -cn $computername.name -scriptblock { get-host } -ErrorAction Stop   # Executes a simple command on all hosts. If this is not successful, the host is ignored in the checks. The runtime of the script is shortened drastically, because there are no timeouts.
        $successfull += $computername.name                                                  # Will only be executed if the check above was successful
    }
    catch {
        $errors += $computername.name
        continue
    }
    finally {
        $Error.Clear()
    }

    # Perform checks and write output
    if ($run_osinfo = 1){
        $osinfo = invoke-command -cn $computername.name -scriptblock { Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' } -ErrorAction Stop
        $osinfo | Select-Object PSComputerName, ProductName, releaseid, currentbuild | Export-Csv ("$outputfile" + "_OS_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }
    
    if ($run_smbinfo = 1){
        $smbinfo = Invoke-Command -cn $computername.name -ScriptBlock { Get-SmbShare | where-object { ( $_.Path ) -and ($_.Description -ne "Default Share") -and ($_.Description -ne "Remote Admin") -and ($_.path -notlike "*LocalsplOnly*") } } -ErrorAction Stop
        $smbinfo | Select-Object PSComputername, Name, Path, Description | Export-Csv ("$outputfile" + "_SMB_Shares_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }

    if ($run_svcinfo = 1){
        $svcinfo = Invoke-Command -cn $computername.name -ScriptBlock { get-wmiobject win32_service | Select-Object name, displayname, startname | where-object { ($_.startname -ne "LocalSystem") -and ($_.startname -notlike "NT AUTHORITY\*") -and ($_.startname -ne $Null) } } -ErrorAction Stop
        $svcinfo | Select-Object PSComputername, name, displayname, startname | Export-Csv ("$outputfile" + "_Services_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }

    if ($run_swinfo = 1){
        $swinfo = Invoke-Command -cn $computername.name -ScriptBlock { Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\uninstall\*' | Select-Object DisplayName, Publisher, DisplayVersion, InstallDate } -ErrorAction Stop
        $swinfo | Select-Object PSComputername, Publisher, DisplayName, DisplayVersion, InstallDate | Export-Csv ("$outputfile" + "_Software_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }

    if ($run_execinfo = 1){
        $execinfo = Invoke-Command -cn $computername.name -ScriptBlock { Get-ExecutionPolicy } -ErrorAction Stop
        $execinfo | Select-Object PSComputername, value | Export-Csv ("$outputfile" + "_Execution_policy_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }

    if ($run_taskinfo = 1){
        $taskinfo = Invoke-Command -cn $computername.name -ScriptBlock { schtasks.exe /query /V /FO CSV | ConvertFrom-CSV | where { ($_.Taskname -ne "TaskName") -and ($_.Taskname -notlike "*Optimize Start Menu Cache Files*") -and ($_."run as user" -notlike "*System") -and ($_."run as user" -ne "LOCAL SERVICE") -and ($_."run as user" -ne "EVERYONE") -and ($_."run as user" -ne "INTERACTIVE") -and ($_."run as user" -ne "Users") -and ($_."run as user" -ne "NETWORK SERVICE") -and ($_."run as user" -ne "Administrators") -and ($_."run as user" -ne "Authenticated Users") } }
        $taskinfo | Select-Object PSComputername, TaskName, Comment, "Task To Run", "Run as User" | Export-Csv ("$outputfile" + "_Scheduled_tasks_infos.csv") -Delimiter ";" -NoTypeInformation -Append
    }

}

# Log schreiben
"Summary`nScanned servers: " + ($computers.count) + "`n" + "of which were successful: " + $successfull.count + "`n" + "of which were not successful: " + $errors.count | Out-File -FilePath $logfile -append   # Very simple composition of strings as summary for the log
"`nUnsucessful scans:" | Out-File -FilePath $logfile -append
$errors | Out-File -FilePath $logfile -append
"`nSuccesful scans" | Out-File -FilePath $logfile -append
$successfull | Out-File -FilePath $logfile -append

# If the following command is enabled, it'll collect the reports and log in a ZIP file (original files remain)
# Compress-Archive -Path (Get-ChildItem -Path $outputfile*)  -DestinationPath ("$outputfile" + ".zip")