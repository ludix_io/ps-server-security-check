# PS Server Security Check

## About

### Description:
This script checks all servers in a specified OU. Then it creates a report per check in the path defined under $outputfiledirdef.

### Checks and reports:
  - Report about OS version and patch level information
  - Report about non-standard SMB shares
  - Report about services which are executed with non-standard accounts
  - Report about installed software and its version
  - Report about PowerShell Execution Policy (ByPass, RemoteSignes etc.)
  - Report about Scheduled Task which are executed with non-standard accounts

## Changelog:
  - Rudimentary basic function (B1)
  - CSV reports improved (B1)
  - After the checks a logfile with summary is written (B1)
  - Checks extended (V1.0)
  - Reports are compressed to ZIP file for easier reuse (V1.0)
  - Checks can now be selected and deselected individually (V 1.1)
  - Performance increased by Try/Catch (V1.1)
  - Logfile now also shows successful systems (V1.1)
  - Code commented (V1.1)
  - Citrix VDA OU can now be defined, also a single VDA if they are identical (V1.2)